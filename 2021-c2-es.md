# ESPANHOL

## 5 TOPICOS

### 5x5

**Topic 01**: dia, feliz, falte, ninguna, cada

**Topic 02**: mujeres, igualdad, derechos, mundo, violencia

**Topic 03**: todas, gracias, felicidades, mundo, aquellas

**Topic 04**: hoy, lucha, siempre, dias, nunca

**Topic 05**: mujer, internacional, ser, dia, marzo

### 5x10

**Topic 01**: dia, feliz, falte, ninguna, cada, algun, buen, celebrar, dejen, especial

**Topic 02**: mujeres, igualdad, derechos, mundo, violencia, hombres, lucha, genero, vida, sociedad

**Topic 03**: todas, gracias, felicidades, mundo, aquellas, voz, queremos, especial, companeras, quiero

**Topic 04**: hoy, lucha, siempre, dias, nunca, ano, igualdad, manana, solo, derechos

**Topic 05**: mujer, internacional, ser, dia, marzo, trabajadora, hombre, vida, igualdad, conmemoracion

### 5x15

**Topic 01**: dia, feliz, falte, ninguna, cada, algun, buen, celebrar, dejen, especial, lucha, podamos, internacional, dias, decir

**Topic 02**: mujeres, igualdad, derechos, mundo, violencia, hombres, lucha, genero, vida, sociedad, asi, mexico, feminismo, pais, ninas

**Topic 03**: todas, gracias, felicidades, mundo, aquellas, voz, queremos, especial, companeras, quiero, muchas, cada, ustedes, luchan, juntas

**Topic 04**: hoy, lucha, siempre, dias, nunca, ano, igualdad, manana, solo, derechos, dia, conmemora, voz, queremos, conmemoramos

**Topic 05**: mujer, internacional, ser, dia, marzo, trabajadora, hombre, vida, igualdad, conmemoracion, sociedad, conmemora, derechos, mundo, cada

### 5x20


**Topic 01**: dia, feliz, falte, ninguna, cada, algun, buen, celebrar, dejen, especial, lucha, podamos, internacional, dias, decir, mejor, celebracion, ojala, luchan, salir

**Topic 02**: mujeres, igualdad, derechos, mundo, violencia, hombres, lucha, genero, vida, sociedad, asi, mexico, feminismo, pais, ninas, trabajo, futuro, historia, solo, respeto

**Topic 03**: todas, gracias, felicidades, mundo, aquellas, voz, queremos, especial, companeras, quiero, muchas, cada, ustedes, luchan, juntas, amigas, hermanas, abrazo, luchando, vida

**Topic 04**: hoy, lucha, siempre, dias, nunca, ano, igualdad, manana, solo, derechos, dia, conmemora, voz, queremos, conmemoramos, ser, quiero, seguimos, miedo, feminista

**Topic 05**: mujer, internacional, ser, dia, marzo, trabajadora, hombre, vida, igualdad, conmemoracion, sociedad, conmemora, derechos, mundo, cada, violencia, genero, madre, historia, conmemoramos

## 10 TOPICOS

### 10x5

**Topic 01**: dia, feliz, falte, cada, ninguna

**Topic 02**: mujeres, mundo, hombres, vida, violencia

**Topic 03**: todas, mundo, felicidades, aquellas, especial

**Topic 04**: hoy, dias, siempre, dia, manana

**Topic 05**: mujer, internacional, dia, marzo, trabajadora

**Topic 06**: ser, puede, debe, mujer, ninguna

**Topic 07**: lucha, feminista, derechos, sigue, conmemora

**Topic 08**: gracias, muchas, ultimo, disponible, siempre

**Topic 09**: miedo, asi, quiero, queremos, libres

**Topic 10**: igualdad, derechos, genero, violencia, respeto

### 10x10

**Topic 01**: dia, feliz, falte, cada, ninguna, buen, algun, celebrar, dejen, dias

**Topic 02**: mujeres, mundo, hombres, vida, violencia, historia, ninas, trabajo, pais, mexico

**Topic 03**: todas, mundo, felicidades, aquellas, especial, luchan, voz, companeras, juntas, abrazo

**Topic 04**: hoy, dias, siempre, dia, manana, ano, voz, conmemora, nunca, conmemoramos

**Topic 05**: mujer, internacional, dia, marzo, trabajadora, conmemoracion, conmemora, hombre, vida, sociedad

**Topic 06**: ser, puede, debe, mujer, ninguna, falte, deberia, vida, hecho, parte

**Topic 07**: lucha, feminista, derechos, sigue, conmemora, viva, continua, conmemoramos, celebra, marzo

**Topic 08**: gracias, muchas, ultimo, disponible, siempre, tan, ustedes, mejor, gran, amo

**Topic 09**: miedo, asi, quiero, queremos, libres, solo, vivas, feminismo, nunca, marcha

**Topic 10**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, luchando, justicia, real

### 10x15

**Topic 01**: dia, feliz, falte, cada, ninguna, buen, algun, celebrar, dejen, dias, especial, mejor, decir, podamos, buenos

**Topic 02**: mujeres, mundo, hombres, vida, violencia, historia, ninas, trabajo, pais, mexico, trabajadoras, trans, futuro, pandemia, igualitario

**Topic 03**: todas, mundo, felicidades, aquellas, especial, luchan, voz, companeras, juntas, abrazo, cada, ustedes, luchando, amigas, hermanas

**Topic 04**: hoy, dias, siempre, dia, manana, ano, voz, conmemora, nunca, conmemoramos, celebra, celebramos, marzo, buenos, solo

**Topic 05**: mujer, internacional, dia, marzo, trabajadora, conmemoracion, conmemora, hombre, vida, sociedad, mundo, marco, celebra, especial, historia

**Topic 06**: ser, puede, debe, mujer, ninguna, falte, deberia, vida, hecho, parte, dejen, quiero, orgullosa, libre, hombre

**Topic 07**: lucha, feminista, derechos, sigue, conmemora, viva, continua, conmemoramos, celebra, marzo, seguimos, sociedad, constante, companeras, dia

**Topic 08**: gracias, muchas, ultimo, disponible, siempre, tan, ustedes, mejor, gran, amo, trabajo, compartir, vida, diario, voz

**Topic 09**: miedo, asi, quiero, queremos, libres, solo, vivas, feminismo, nunca, marcha, siempre, feminista, cada, ano, casa

**Topic 10**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, luchando, justicia, real, equidad, libertad, hombres, seguir, mundo

### 10x20

**Topic 01**: dia, feliz, falte, cada, ninguna, buen, algun, celebrar, dejen, dias, especial, mejor, decir, podamos, buenos, luchan, luchadoras, celebracion, guerreras, ojala

**Topic 02**: mujeres, mundo, hombres, vida, violencia, historia, ninas, trabajo, pais, mexico, trabajadoras, trans, futuro, pandemia, igualitario, derechos, marzo, parte, lideres, muchas

**Topic 03**: todas, mundo, felicidades, aquellas, especial, luchan, voz, companeras, juntas, abrazo, cada, ustedes, luchando, amigas, hermanas, respeto, queremos, vida, reconocimiento, faltan

**Topic 04**: hoy, dias, siempre, dia, manana, ano, voz, conmemora, nunca, conmemoramos, celebra, celebramos, marzo, buenos, solo, recordar, ayer, celebrar, felicita, buen

**Topic 05**: mujer, internacional, dia, marzo, trabajadora, conmemoracion, conmemora, hombre, vida, sociedad, mundo, marco, celebra, especial, historia, gran, conmemoramos, felicidades, participacion, cada

**Topic 06**: ser, puede, debe, mujer, ninguna, falte, deberia, vida, hecho, parte, dejen, quiero, orgullosa, libre, hombre, libres, solo, humano, ultima, dias

**Topic 07**: lucha, feminista, derechos, sigue, conmemora, viva, continua, conmemoramos, celebra, marzo, seguimos, sociedad, constante, companeras, dia, pie, felicita, diaria, conmemoracion, equidad

**Topic 08**: gracias, muchas, ultimo, disponible, siempre, tan, ustedes, mejor, gran, amo, trabajo, compartir, vida, diario, voz, companeras, apoyo, existir, mil, parte

**Topic 09**: miedo, asi, quiero, queremos, libres, solo, vivas, feminismo, nunca, marcha, siempre, feminista, cada, ano, casa, ver, vivir, hace, salir, calle

**Topic 10**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, luchando, justicia, real, equidad, libertad, hombres, seguir, mundo, compromiso, futuro, feminismo, trabajando, hacia

## 15 TOPICOS

### 15x5

**Topic 01**: feliz, falte, ninguna, dia, dejen

**Topic 02**: mujeres, hombres, vida, violencia, historia

**Topic 03**: todas, felicidades, aquellas, especial, voz

**Topic 04**: hoy, nunca, manana, voz, conmemora

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre

**Topic 06**: ser, puede, debe, ninguna, solo

**Topic 07**: lucha, feminista, derechos, sigue, viva

**Topic 08**: gracias, muchas, ultimo, disponible, tan

**Topic 09**: dia, internacional, cada, celebrar, buen

**Topic 10**: igualdad, derechos, genero, violencia, respeto

**Topic 11**: asi, marcha, feminista, feminismo, gobierno

**Topic 12**: siempre, juntas, manana, fuertes, nunca

**Topic 13**: miedo, queremos, quiero, libres, vivas

**Topic 14**: dias, ano, buenos, solo, marzo

**Topic 15**: mundo, mejor, igualitario, futuro, justo

### 15x10

**Topic 01**: feliz, falte, ninguna, dia, dejen, decir, companeras, deseamos, desea, luchadoras

**Topic 02**: mujeres, hombres, vida, violencia, historia, trabajo, trabajadoras, ninas, pais, derechos

**Topic 03**: todas, felicidades, aquellas, especial, voz, companeras, luchan, abrazo, amigas, juntas

**Topic 04**: hoy, nunca, manana, voz, conmemora, conmemoramos, celebra, celebramos, ayer, recordar

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre, vida, sociedad, conmemora, conmemoracion, historia

**Topic 06**: ser, puede, debe, ninguna, solo, falte, deberia, vida, hecho, mujer

**Topic 07**: lucha, feminista, derechos, sigue, viva, conmemora, continua, conmemoramos, celebra, seguimos

**Topic 08**: gracias, muchas, ultimo, disponible, tan, ustedes, gran, amo, vida, trabajo

**Topic 09**: dia, internacional, cada, celebrar, buen, algun, conmemorar, conmemoracion, celebracion, especial

**Topic 10**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, justicia, luchando, real

**Topic 11**: asi, marcha, feminista, feminismo, gobierno, mexico, palacio, nacional, feministas, solo

**Topic 12**: siempre, juntas, manana, fuertes, nunca, respeto, ustedes, lado, amo, admiracion

**Topic 13**: miedo, queremos, quiero, libres, vivas, vivir, salir, calle, casa, tener

**Topic 14**: dias, ano, buenos, solo, marzo, hace, pasado, cada, sino, resto

**Topic 15**: mundo, mejor, igualitario, futuro, justo, cada, lideres, covid, hacer, construir

### 15x15

**Topic 01**: feliz, falte, ninguna, dia, dejen, decir, companeras, deseamos, desea, luchadoras, guerreras, chicas, matarnos, dice, mujer

**Topic 02**: mujeres, hombres, vida, violencia, historia, trabajo, trabajadoras, ninas, pais, derechos, trans, pandemia, mexico, valientes, muchas

**Topic 03**: todas, felicidades, aquellas, especial, voz, companeras, luchan, abrazo, amigas, juntas, ustedes, luchando, hermanas, cada, respeto

**Topic 04**: hoy, nunca, manana, voz, conmemora, conmemoramos, celebra, celebramos, ayer, recordar, aqui, felicita, celebrar, marzo, solo

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre, vida, sociedad, conmemora, conmemoracion, historia, marco, participacion, primera, conmemoramos, madre

**Topic 06**: ser, puede, debe, ninguna, solo, falte, deberia, vida, hecho, mujer, parte, dejen, quiero, tan, hombre

**Topic 07**: lucha, feminista, derechos, sigue, viva, conmemora, continua, conmemoramos, celebra, seguimos, marzo, sociedad, constante, companeras, pie

**Topic 08**: gracias, muchas, ultimo, disponible, tan, ustedes, gran, amo, vida, trabajo, compartir, voz, diario, cada, companeras

**Topic 09**: dia, internacional, cada, celebrar, buen, algun, conmemorar, conmemoracion, celebracion, especial, recordar, marzo, luchan, felicitar, ojala

**Topic 10**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, justicia, luchando, real, equidad, libertad, hombres, seguir, compromiso

**Topic 11**: asi, marcha, feminista, feminismo, gobierno, mexico, palacio, nacional, feministas, solo, hace, ver, nunca, aqui, bien

**Topic 12**: siempre, juntas, manana, fuertes, nunca, respeto, ustedes, lado, amo, admiracion, unidas, ahora, sido, amor, vida

**Topic 13**: miedo, queremos, quiero, libres, vivas, vivir, salir, calle, casa, tener, juntas, podamos, libre, seguras, justicia

**Topic 14**: dias, ano, buenos, solo, marzo, hace, pasado, cada, sino, resto, semana, lunes, seguimos, respeto, vida

**Topic 15**: mundo, mejor, igualitario, futuro, justo, cada, lideres, covid, hacer, construir, cambiar, lugar, ninas, equitativo, luchan

### 15x20

**Topic 01**: feliz, falte, ninguna, dia, dejen, decir, companeras, deseamos, desea, luchadoras, guerreras, chicas, matarnos, dice, mujer, hermosas, happy, menos, lunes, deseo

**Topic 02**: mujeres, hombres, vida, violencia, historia, trabajo, trabajadoras, ninas, pais, derechos, trans, pandemia, mexico, valientes, muchas, parte, marzo, grandes, especial, sociedad

**Topic 03**: todas, felicidades, aquellas, especial, voz, companeras, luchan, abrazo, amigas, juntas, ustedes, luchando, hermanas, cada, respeto, vida, faltan, reconocimiento, justicia, tocan

**Topic 04**: hoy, nunca, manana, voz, conmemora, conmemoramos, celebra, celebramos, ayer, recordar, aqui, felicita, celebrar, marzo, solo, seguimos, aquellas, anos, recordamos, luchando

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre, vida, sociedad, conmemora, conmemoracion, historia, marco, participacion, primera, conmemoramos, madre, motivo, fuerte, gran, papel, celebra

**Topic 06**: ser, puede, debe, ninguna, solo, falte, deberia, vida, hecho, mujer, parte, dejen, quiero, tan, hombre, orgullosa, nadie, libre, tener, humano

**Topic 07**: lucha, feminista, derechos, sigue, viva, conmemora, continua, conmemoramos, celebra, seguimos, marzo, sociedad, constante, companeras, pie, felicita, diaria, equidad, fuerza, conmemoracion

**Topic 08**: gracias, muchas, ultimo, disponible, tan, ustedes, gran, amo, vida, trabajo, compartir, voz, diario, cada, companeras, mejor, existir, apoyo, mil, dar

**Topic 09**: dia, internacional, cada, celebrar, buen, algun, conmemorar, conmemoracion, celebracion, especial, recordar, marzo, luchan, felicitar, ojala, mejor, tan, luchar, importante, reflexion

**Topic 10**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, justicia, luchando, real, equidad, libertad, hombres, seguir, compromiso, feminismo, trabajando, hacia, camino, seguimos

**Topic 11**: asi, marcha, feminista, feminismo, gobierno, mexico, palacio, nacional, feministas, solo, hace, ver, nunca, aqui, bien, movimiento, anos, hacer, violencia, presidente

**Topic 12**: siempre, juntas, manana, fuertes, nunca, respeto, ustedes, lado, amo, admiracion, unidas, ahora, sido, amor, vida, ahi, apoyo, tan, hermanas, casa

**Topic 13**: miedo, queremos, quiero, libres, vivas, vivir, salir, calle, casa, tener, juntas, podamos, libre, seguras, justicia, fuertes, nunca, caminar, sola, volver

**Topic 14**: dias, ano, buenos, solo, marzo, hace, pasado, cada, sino, resto, semana, lunes, seguimos, respeto, vida, tod, felicidades, aunque, restantes, aires

**Topic 15**: mundo, mejor, igualitario, futuro, justo, cada, lideres, covid, hacer, construir, cambiar, lugar, ninas, equitativo, luchan, iguales, esfuerzos, amor, socialmente, entero

## 20 TOPICOS

### 20x5

**Topic 01**: feliz, falte, ninguna, dia, dejen

**Topic 02**: mujeres, hombres, derechos, historia, trabajadoras

**Topic 03**: todas, felicidades, aquellas, especial, voz

**Topic 04**: hoy, manana, conmemora, voz, conmemoramos

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre

**Topic 06**: ser, debe, puede, ninguna, falte

**Topic 07**: lucha, conmemora, sigue, viva, derechos

**Topic 08**: gracias, muchas, disponible, ultimo, ustedes

**Topic 09**: dia, internacional, cada, celebrar, buen

**Topic 10**: igualdad, derechos, respeto, oportunidades, sociedad

**Topic 11**: cada, solo, nunca, ano, aqui

**Topic 12**: siempre, juntas, manana, fuertes, respeto

**Topic 13**: miedo, quiero, vivir, salir, calle

**Topic 14**: dias, buenos, ano, marzo, resto

**Topic 15**: mundo, mejor, igualitario, futuro, justo

**Topic 16**: asi, ojala, mexico, palacio, aun

**Topic 17**: violencia, genero, libre, machista, vida

**Topic 18**: queremos, vivas, libres, fuertes, iguales

**Topic 19**: feminismo, machismo, trans, hombres, movimiento

**Topic 20**: feminista, marcha, gobierno, movimiento, palacio

### 20x10

**Topic 01**: feliz, falte, ninguna, dia, dejen, decir, companeras, deseamos, desea, luchadoras

**Topic 02**: mujeres, hombres, derechos, historia, trabajadoras, vida, ninas, trabajo, pais, trans

**Topic 03**: todas, felicidades, aquellas, especial, voz, luchan, companeras, abrazo, juntas, amigas

**Topic 04**: hoy, manana, conmemora, voz, conmemoramos, celebra, celebramos, ayer, nunca, recordar

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre, sociedad, conmemora, conmemoracion, vida, participacion

**Topic 06**: ser, debe, puede, ninguna, falte, deberia, mujer, hecho, dejen, parte

**Topic 07**: lucha, conmemora, sigue, viva, derechos, continua, conmemoramos, celebra, sociedad, marzo

**Topic 08**: gracias, muchas, disponible, ultimo, ustedes, gran, tan, compartir, amo, diario

**Topic 09**: dia, internacional, cada, celebrar, buen, algun, conmemoracion, conmemorar, celebracion, especial

**Topic 10**: igualdad, derechos, respeto, oportunidades, sociedad, real, luchando, libertad, hombres, justicia

**Topic 11**: cada, solo, nunca, ano, aqui, hace, vez, hacer, anos, vida

**Topic 12**: siempre, juntas, manana, fuertes, respeto, ustedes, lado, admiracion, unidas, sido

**Topic 13**: miedo, quiero, vivir, salir, calle, casa, tener, libres, libre, podamos

**Topic 14**: dias, buenos, ano, marzo, resto, lunes, pasado, semana, felicidades, sino

**Topic 15**: mundo, mejor, igualitario, futuro, justo, lideres, covid, construir, equitativo, cambiar

**Topic 16**: asi, ojala, mexico, palacio, aun, nacional, cosas, gobierno, bien, vivio

**Topic 17**: violencia, genero, libre, machista, vida, equidad, justicia, victimas, desigualdad, discriminacion

**Topic 18**: queremos, vivas, libres, fuertes, iguales, seguras, juntas, valientes, justicia, unidas

**Topic 19**: feminismo, machismo, trans, hombres, movimiento, representa, hombre, interseccional, necesario, inclusivo

**Topic 20**: feminista, marcha, gobierno, movimiento, palacio, nacional, mexico, feministas, presidente, ciudad

### 20x15

**Topic 01**: feliz, falte, ninguna, dia, dejen, decir, companeras, deseamos, desea, luchadoras, chicas, guerreras, matarnos, dice, mujer

**Topic 02**: mujeres, hombres, derechos, historia, trabajadoras, vida, ninas, trabajo, pais, trans, mexico, valientes, marzo, pandemia, grandes

**Topic 03**: todas, felicidades, aquellas, especial, voz, luchan, companeras, abrazo, juntas, amigas, ustedes, hermanas, luchando, respeto, faltan

**Topic 04**: hoy, manana, conmemora, voz, conmemoramos, celebra, celebramos, ayer, nunca, recordar, felicita, celebrar, marzo, aquellas, recordamos

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre, sociedad, conmemora, conmemoracion, vida, participacion, marco, historia, conmemoramos, primera, motivo

**Topic 06**: ser, debe, puede, ninguna, falte, deberia, mujer, hecho, dejen, parte, orgullosa, humano, hombre, ultima, deben

**Topic 07**: lucha, conmemora, sigue, viva, derechos, continua, conmemoramos, celebra, sociedad, marzo, constante, seguimos, companeras, pie, felicita

**Topic 08**: gracias, muchas, disponible, ultimo, ustedes, gran, tan, compartir, amo, diario, trabajo, companeras, voz, existir, mil

**Topic 09**: dia, internacional, cada, celebrar, buen, algun, conmemoracion, conmemorar, celebracion, especial, recordar, marzo, luchan, felicitar, ojala

**Topic 10**: igualdad, derechos, respeto, oportunidades, sociedad, real, luchando, libertad, hombres, justicia, seguir, genero, equidad, compromiso, trabajando

**Topic 11**: cada, solo, nunca, ano, aqui, hace, vez, hacer, anos, vida, ver, tan, bien, seguimos, nadie

**Topic 12**: siempre, juntas, manana, fuertes, respeto, ustedes, lado, admiracion, unidas, sido, amo, apoyo, amor, ahora, adelante

**Topic 13**: miedo, quiero, vivir, salir, calle, casa, tener, libres, libre, podamos, sola, caminar, volver, poder, cancion

**Topic 14**: dias, buenos, ano, marzo, resto, lunes, pasado, semana, felicidades, sino, solo, tod, aires, restantes, hace

**Topic 15**: mundo, mejor, igualitario, futuro, justo, lideres, covid, construir, equitativo, cambiar, ninas, lugar, hacer, luchan, iguales

**Topic 16**: asi, ojala, mexico, palacio, aun, nacional, cosas, gobierno, bien, vivio, luce, feministas, manifestacion, zocalo, violadores

**Topic 17**: violencia, genero, libre, machista, vida, equidad, justicia, victimas, desigualdad, discriminacion, hacia, perspectiva, ninas, sexual, pais

**Topic 18**: queremos, vivas, libres, fuertes, iguales, seguras, juntas, valientes, justicia, unidas, flores, felices, felicitar, menos, felicitaciones

**Topic 19**: feminismo, machismo, trans, hombres, movimiento, representa, hombre, interseccional, necesario, inclusivo, feministas, radical, patriarcado, odio, clase

**Topic 20**: feminista, marcha, gobierno, movimiento, palacio, nacional, mexico, feministas, presidente, ciudad, revolucion, policias, zocalo, vallas, muro

### 20x20

**Topic 01**: feliz, falte, ninguna, dia, dejen, decir, companeras, deseamos, desea, luchadoras, chicas, guerreras, matarnos, dice, mujer, hermosas, menos, happy, deseo, lunes

**Topic 02**: mujeres, hombres, derechos, historia, trabajadoras, vida, ninas, trabajo, pais, trans, mexico, valientes, marzo, pandemia, grandes, especial, muchas, reconocimiento, parte, sociedad

**Topic 03**: todas, felicidades, aquellas, especial, voz, luchan, companeras, abrazo, juntas, amigas, ustedes, hermanas, luchando, respeto, faltan, reconocimiento, justicia, tocan, saludo, luchadoras

**Topic 04**: hoy, manana, conmemora, voz, conmemoramos, celebra, celebramos, ayer, nunca, recordar, felicita, celebrar, marzo, aquellas, recordamos, especial, lucharon, luchamos, luchar, buen

**Topic 05**: mujer, internacional, marzo, trabajadora, hombre, sociedad, conmemora, conmemoracion, vida, participacion, marco, historia, conmemoramos, primera, motivo, papel, celebra, gran, celebramos, madre

**Topic 06**: ser, debe, puede, ninguna, falte, deberia, mujer, hecho, dejen, parte, orgullosa, humano, hombre, ultima, deben, libres, gana, podamos, libre, tan

**Topic 07**: lucha, conmemora, sigue, viva, derechos, continua, conmemoramos, celebra, sociedad, marzo, constante, seguimos, companeras, pie, felicita, equidad, diaria, conmemoracion, reivindicacion, aun

**Topic 08**: gracias, muchas, disponible, ultimo, ustedes, gran, tan, compartir, amo, diario, trabajo, companeras, voz, existir, mil, apoyo, dar, mejor, parte, marchar

**Topic 09**: dia, internacional, cada, celebrar, buen, algun, conmemoracion, conmemorar, celebracion, especial, recordar, marzo, luchan, felicitar, ojala, mejor, reflexion, trabajadora, importante, luchar

**Topic 10**: igualdad, derechos, respeto, oportunidades, sociedad, real, luchando, libertad, hombres, justicia, seguir, genero, equidad, compromiso, trabajando, conmemoramos, camino, reconocimiento, luchar, seguimos

**Topic 11**: cada, solo, nunca, ano, aqui, hace, vez, hacer, anos, vida, ver, tan, bien, seguimos, nadie, aun, voz, vamos, menos, ahora

**Topic 12**: siempre, juntas, manana, fuertes, respeto, ustedes, lado, admiracion, unidas, sido, amo, apoyo, amor, ahora, adelante, ahi, hermanas, ayer, casa, presente

**Topic 13**: miedo, quiero, vivir, salir, calle, casa, tener, libres, libre, podamos, sola, caminar, volver, poder, cancion, manana, amigas, regresar, ninguna, mama

**Topic 14**: dias, buenos, ano, marzo, resto, lunes, pasado, semana, felicidades, sino, solo, tod, aires, restantes, hace, deberian, luchamos, respeto, femicidios, demas

**Topic 15**: mundo, mejor, igualitario, futuro, justo, lideres, covid, construir, equitativo, cambiar, ninas, lugar, hacer, luchan, iguales, mexico, oportunidades, esfuerzos, socialmente, entero

**Topic 16**: asi, ojala, mexico, palacio, aun, nacional, cosas, gobierno, bien, vivio, luce, feministas, manifestacion, zocalo, violadores, claro, vive, pais, gente, ciudad

**Topic 17**: violencia, genero, libre, machista, vida, equidad, justicia, victimas, desigualdad, discriminacion, hacia, perspectiva, ninas, sexual, pais, erradicar, sociedad, mexico, feminicidios, tipo

**Topic 18**: queremos, vivas, libres, fuertes, iguales, seguras, juntas, valientes, justicia, unidas, flores, felices, felicitar, menos, felicitaciones, vivir, trabajo, miedo, reconocer, totalmente

**Topic 19**: feminismo, machismo, trans, hombres, movimiento, representa, hombre, interseccional, necesario, inclusivo, feministas, radical, patriarcado, odio, clase, libertad, social, nunca, incluye, antirracista

**Topic 20**: feminista, marcha, gobierno, movimiento, palacio, nacional, mexico, feministas, presidente, ciudad, revolucion, policias, zocalo, vallas, muro, historia, manifestacion, manifestantes, via, cdmx