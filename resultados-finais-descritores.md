# 2020

## CENÁRIO 1

### PORTUGUÊS (5X15)

**Marco histórico**: marco, mulheres, tecelagem, trabalhavam, trancadas, carbonizadas, aproximadamente, estarem, fabrica, morreram, york, greve, nova, declarou, dia

**Homenagens cotidianas**: onibus, lembrancinha, enfeita, entram, motorista, semana, dar, ano, mulheres, internacional, mulher, dia, feliz, hoje, parabens

**Sarcasmo**: faltar, pode, internacional, mulher, dia, feliz, marco, feministo, depositar, 10k, conta, algum, obrigado, homem, hoje

**Sororidade**: incriveis, mulheres, feliz, esquecam, dia, sonham, brisa, sorte, manas, querem, dao, donas, tudo, fazem, nunca

**Prestar tributo**: feliz, dia, mulher, iconico, internacional, importancia, relembrar, video, vamos, feminista, tok, tik, possivel, hoje, sobre

### ESPANHOL (10x15)

**Comemoração**: feliz, falte, ninguna, dia, ser, todas, dias, estan, fiesta, volvieron, debemos, amigues, super, liga, proximo, matarnos, mujeres, nunca, social, parte

**Memória**: hoy, todas, lucha, vamos, mundo, aquellas, vano, pudieron, sepan, llena, memoria, mierda, cambiar, sido, siempre, pais, manana, queremos, dias, historia

**Marco histórico**: vivas, quemadas, huelga, marzo, gozamos, trabajadoras, valia, festejo, lograr, conmemoracion, derechos, ahora, respeto, hacer, queremos, libres, miedo, pedir, igualdad, sabado

**Desejos e anseios**: quiero, tranquila, caminar, flores, calle, poder, ser, puedo, jirafa, aprendi, diverti, miedo, libres, casa, salir, libre, queremos, escoltadas, solas, salvo

**Lutas diárias**: dia, mujer, internacional, feliz, dejen, paz, vivir, hoy, ser, trabajadora, cada, marzo, historico, conmemora, igualdad, solo, lucha, hombre, luchando, celebra

**Manifestações**: mujeres, marcha, asi, chile, feminista, millon, millones, igualdad, todas, hombres, violencia, calles, mexico, santiago, derechos, feminismo, marchando, zocalo, solo, san

**Protestos**: encapuchadas, realizaban, vandalizaban, contiene, estatua, artes, madero, aplauso, delincuentes, destrozos, bellas, gran, mujer, respetos, destruian, empoderada, enfrento, ocultar, rostro, equivoquen

**Violência de gênero**: gracias, elaboro, violencia, recordarla, visible, debo, pago, recibe, maltrato, vida, machista, ley, homenaje, hacer, hoy, genero, historia, dias, vez, hace

**Sororidade**: jajaja, nunca, unidas, sororidad, vencidas, follo, fea, ano, novio, tias, hermanas, resto, quiero, volvieron, debemos, dias, juntas, manifestantes, mas, amo

**Papéis familiares**: yang, cristina, deseen, tenerlos, creo, padres, deben, ninos, odio, tener, respeto, merecemos, ser, admiro, igualdad, necesito, miedo, hermana, hijos, queria

### INGLÊS (5x20)

**Sororidade**: happy, day, everyday, ladies, women, everyone, spirit, friends, sisters, womanhood, ears, iwd, dear, define, ready, less, stay, incredible, beautiful, queen

**Celebração da mulher**: women, world, day, amp, celebrate, amazing, strong, celebrating, make, life, let, love, power, girls, incredible, proud, around, thank, work, rights

**Características da mulher**: international, day, womens, fine, dedicating, woman, women, every, slaves, fighters, ladies, womxn, gigi, bryant, lahat, precious, inventing, heroine, lorde, pure

**Equidade**: today, justice, every, equality, gender, equal, equity, celebrations, societies, congratulations, recognise, remember, far, climate, want, without, still, men, day, women

**Política**: respect, elect, black, woman, believe, one, position, vote, please, every, love, fine, dedicating, give, man, never, believed, support, superpower, right

## CENÁRIO 2

### Português (5x15)

**Festejo**: mulher, internacional, dia, marco, homenagem, especial, bom, via, amanha, mae, domingo, comemoracao, comemorar, presente, vida

**Batalhas**: mulheres, dia, vida, guerreiras, luta, homenagem, incriveis, direitos, fortes, contra, especial, brasil, homens, marco, outras

**Elogios**: feliz, dia, guerreiras, vcs, dias, bom, maravilhosas, incriveis, deseja, lindas, desejar, merecem, linda, fortes, sempre

**Empoderamento**: ser, mulher, quero, vai, homem, tudo, pode, vida, quiser, sim, sempre, ter, gente, deve, fazer

**Felicitações**: parabens, guerreiras, dia, vcs, marco, especial, deus, bom, lindas, brasil, dias, mulherada, sempre, batalhadoras, fortes

### Espanhol (5x20)

**Celebração**: dia, mujer, feliz, internacional, ser, marzo, trabajadora, cada, lucha, mundo, conmemora, igualdad, especial, vida, hombre, dias, celebrar, conmemoracion, celebra, dejen

**Luta histórica**: mujeres, igualdad, derechos, hombres, mundo, violencia, lucha, genero, vida, sociedad, respeto, miles, solo, pais, historia, mexico, feminismo, calles, trabajo, ninas

**Felicitação**: todas, gracias, feliz, felicidades, mundo, juntas, especial, amigas, companeras, aquellas, muchas, ustedes, abrazo, mejor, voz, quiero, vida, hermanas, queremos, luchan

**Desejos e anseios**: hoy, siempre, lucha, dias, manana, nunca, dia, igualdad, derechos, ano, queremos, solo, voz, historia, calles, juntas, miedo, quiero, calle, ayer

**Protesto**: marcha, ser, asi, feminista, mujer, ver, quiero, manana, solo, miedo, primera, chile, bien, mexico, personas, violencia, mil, tan, ahora, gobierno

### Inglês (5x20)

**Celebração**: day, international, womens, women, every, march, special, year, celebrates, via, celebration, celebrated, 8th, theme, great, celebrating, one, single, wishes, enjoy

**Apoio**: women, strong, celebrating, men, life, may, rights, know, day, incredible, support, many, inspire, power, proud, inspiring, wonderful, powerful, respect, lives

**Felicitações**: happy, ladies, beautiful, day, iwd, everyone, wishing, wishes, keep, wish, wonderful, strong, birthday, lovely, sisters, team, queens, friends, female, favorite

**Equidade**: world, equal, enabled, gender, around, equality, let, better, make, place, create, help, change, girls, together, theme, achievements, bias, collectively, action

**Empoderamento**: woman, every, strong, one, life, man, beautiful, know, proud, day, may, like, single, girl, mother, powerful, never, best, first, without

# 2021

## CENÁRIO 1

### PORTUGUÊS (5x20)

**Felicitações**: feliz, dia, mulher, internacional, mulheres, hoje, lula, marco, mae, parabens, fachin, especial, ser, luta, mundo, respeito, dias, afinal, boca, povo

**Política**: mulher, compromissos, declaracao, assinaram, aceitou, assinar, assume, paises, humanos, onu, conselho, vergonha, saude, defesa, bolsonaro, governo, direitos, brasil, internacional, dia

**Marco histórico**: marco, mulheres, carbonizadas, trancadas, tecelagem, trabalhavam, declarou, estarem, morreram, aproximadamente, york, greve, fabrica, nova, onde, homenagem, dia, bom, lembrar, sempre

**Campanhas**: lindo, sisters, avancarem, cenas, fechamos, apoiando, valor, sabe, inspira, video, juntas, outras, marco, mulher, lula, vai, lembro, milagre, internacional, mundo

**Lutas diárias**: god, woman, fazendo, continuar, vamos, historia, mulheres, hoje, luta, queria, mundo, dizer, feliz, dia, lutaram, fortes, melhor, lutam, parabens, tudo

### Espanhol (5x20)

**Denuncia**: feliz, dia, falte, ninguna, ser, podamos, nadie, dejen, logro, legal, aborto, primer, matarnos, salir, haga, aquellas, tranquilas, calle, decime, paren

**Resistência**: miedo, crecieron, sembraron, alas, libres, vivir, cancion, vivas, juntas, exagerado, casa, corten, razon, calma, ser, quitan, callen, existe, tiemble, cielos

**Tributo**: buen, recordar, video, hoy, dia, creo, momento, hace, ano, manifestantes, difunden, indicaciones, dando, siempre, zocalo, aquellas, dias, protestas, conmemora, celebramos

**Celebração**: mujer, dia, internacional, marzo, ser, trabajadora, felicidades, cada, conmemoracion, muchas, feliz, ilumina, marco, hoy, mejor, morado, gana, conmemorar, celebrar, dias

**Violência**: escobedo, marisela, hija, olvidemos, lucho, enfrento, impune, asesinada, narcos, feminicida, gobernador, quedo, red, nunca, hizo, feminicidio, gobierno, frente, palacio, criando

### Inglês (10x20)

**Empoderamento**: happy, day, beautiful, ladies, westview, powerful, bitches, wishing, female, everyone, bad, character, fiction, iwd, looked, queens, women, woman, amazing, incredible

**Manifestações em redes sociais**: international, day, women, march, men, stepping, like, congratulations, great, ladies, help, one, feel, badge, earned, petitions, tweets, name, hope, pretty

**Ativismo interseccional**: pleasant, whatever, reason, trans, able, wish, come, girls, international, reminder, women, help, black, petitions, free, feel, tweets, leave, thinking, simple

**Celebração**: women, celebrating, support, strong, trans, work, world, westview, respect, life, incredible, many, across, show, rights, amazing, power, empowerment, great, today

**Inspiração**: woman, god, abundance, mother, wonderful, ever, teachings, earth, thank, influential, stands, growing, guidance, care, courage, name, life, strength, wonder, man

**Música**: amazing, womens, playlist, curated, stream, world, honor, celebrate, around, bitches, bad, achievements, ros, today, songs, make, challenge, better, music, work

**Homenagem à profissionais de saúde**: thank, around, world, continued, keeping, healthy, supports, vital, tirelessly, worked, services, special, ensure, safe, covid, young, let, girls, celebrate, women

**Mães solo**: every, today, day, strength, celebrate, everyday, inspire, life, screen, continually, single, incredible, shine, make, queen, lives, join, princess, sendin, without

**Desejos de mudança**: amp, gender, let, equality, future, challenge, female, join, equal, change, celebrate, powerful, world, beautiful, choose, better, make, girls, history, bias

**Criação/educação**: love, may, strong, know, much, beautiful, raise, ladies, queens, queen, mother, thank, sendin, mad, praises, princess, always, respect, deserve, world

## CENÁRIO 2

### Português (5x20)

**Felicitações**: mulher, internacional, dia, hoje, ser, homenagem, especial, parabens, continuacao, presente, via, sobre, mae, melhor, homem, lula, onde, brasil, quiser, lugar

**Empoderamento**: mulheres, parabens, mundo, dia, luta, hoje, dias, vida, respeito, incriveis, especial, forca, direitos, ser, igualdade, guerreiras, sempre, fortes, aqui, homenagem

**Lutas**: marco, dia, luta, lula, hoje, ano, data, respeito, dias, ainda, sobre, flores, brasil, gente, feira, ser, anos, apenas, vai, segunda

**Elogios**: feliz, dia, guerreiras, incriveis, woman, vcs, god, deseja, dias, bom, maravilhosas, desejar, sempre, mulherada, instagram, lindas, fortes, amor, mulheres, seguidoras

**Afeto**: amo, tanto, amor, demais, tudo, vida, deus, vcs, liam, video, aqui, ser, gente, mae, obrigada, linda, camila, sempre, cara, lindo

### Espanhol (10x20)

**Celebração**: dia, feliz, falte, cada, ninguna, buen, algun, celebrar, dejen, dias, especial, mejor, decir, podamos, buenos, luchan, luchadoras, celebracion, guerreras, ojala

**Opressão**: mujeres, mundo, hombres, vida, violencia, historia, ninas, trabajo, pais, mexico, trabajadoras, trans, futuro, pandemia, igualitario, derechos, marzo, parte, lideres, muchas

**Demandas**: todas, mundo, felicidades, aquellas, especial, luchan, voz, companeras, juntas, abrazo, cada, ustedes, luchando, amigas, hermanas, respeto, queremos, vida, reconocimiento, faltan

**Celebração 2**: hoy, dias, siempre, dia, manana, ano, voz, conmemora, nunca, conmemoramos, celebra, celebramos, marzo, buenos, solo, recordar, ayer, celebrar, felicita, buen

**Celebração 3**: mujer, internacional, dia, marzo, trabajadora, conmemoracion, conmemora, hombre, vida, sociedad, mundo, marco, celebra, especial, historia, gran, conmemoramos, felicidades, participacion, cada

**Direitos**: ser, puede, debe, mujer, ninguna, falte, deberia, vida, hecho, parte, dejen, quiero, orgullosa, libre, hombre, libres, solo, humano, ultima, dias

**Celebração 4**: lucha, feminista, derechos, sigue, conmemora, viva, continua, conmemoramos, celebra, marzo, seguimos, sociedad, constante, companeras, dia, pie, felicita, diaria, conmemoracion, equidad

**Suporte**: gracias, muchas, ultimo, disponible, siempre, tan, ustedes, mejor, gran, amo, trabajo, compartir, vida, diario, voz, companeras, apoyo, existir, mil, parte

**Desejos**: miedo, asi, quiero, queremos, libres, solo, vivas, feminismo, nunca, marcha, siempre, feminista, cada, ano, casa, ver, vivir, hace, salir, calle

**Justiça**: igualdad, derechos, genero, violencia, respeto, oportunidades, sociedad, luchando, justicia, real, equidad, libertad, hombres, seguir, mundo, compromiso, futuro, feminismo, trabajando, hacia

### Inglês (10x20)

**Felicitações**: day, international, women, every, march, womens, celebration, via, celebrating, wishes, special, celebrated, 8th, celebrates, good, men, occasion, year, wish, cultural

**Empoderamento**: women, amazing, work, celebrating, life, many, proud, great, incredible, support, team, make, men, like, one, working, inspiring, girls, rights, inspire

**Desejos**: happy, ladies, iwd, everyone, wishing, wonderful, beautiful, wish, womens, wishes, queens, lovely, keep, world, friends, incredible, female, team, favorite, birthday

**Admiração**: woman, every, god, one, life, day, man, like, single, proud, first, mother, another, best, respect, magic, without, power, inspires, know

**Desafios**: world, challenge, gender, choose, around, change, let, bias, inequality, comes, equal, inclusive, theme, alert, challenged, equality, call, achievements, help, create

**Apoio no trabalho/emprego**: amp, gender, female, girls, support, join, equality, thanks, work, leaders, new, share, leadership, health, event, read, time, live, future, many

**Equidade**: today, celebrate, every, achievements, everyday, let, celebrating, day, join, female, political, social, cultural, economic, equality, take, lives, contributions, incredible, recognize

**Respeito**: love, much, respect, ladies, life, always, see, sending, beautiful, show, thanks, appreciate, one, deserve, would, mom, good, best, girls, queens

**Inspiração**: thank, much, great, wonderful, work, inspiring, support, everything, like, sharing, want, say, better, ladies, life, make, inspiration, making, everyone, would

**Criação/educação**: strong, beautiful, may, know, raise, powerful, women, ladies, independent, brave, stay, always, smart, intelligent, world, life, keep, inspiring, way, strength

