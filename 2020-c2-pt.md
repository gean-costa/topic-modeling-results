# PORTUGUÊS

## 5 TÓPICOS

### 5x5

**Topic 01**: mulher, internacional, dia, marco, homenagem

**Topic 02**: mulheres, parabens, guerreiras, dia, mundo

**Topic 03**: feliz, dia, guerreiras, dias, mundo

**Topic 04**: hoje, ser, sororidade, dias, luta

**Topic 05**: amo, video, tanto, demais, vida

### 5x10

**Topic 01**: mulher, internacional, dia, marco, homenagem, parabens, bom, especial, via, hoje

**Topic 02**: mulheres, parabens, guerreiras, dia, mundo, luta, brasil, vida, especial, incriveis

**Topic 03**: feliz, dia, guerreiras, dias, mundo, vcs, incriveis, mulheres, bom, maravilhosas

**Topic 04**: hoje, ser, sororidade, dias, luta, respeito, sempre, tudo, quero, gente

**Topic 05**: amo, video, tanto, demais, vida, tudo, deus, melhor, perfeito, mae

### 5x15

**Topic 01**: mulher, internacional, dia, marco, homenagem, parabens, bom, especial, via, hoje, amanha, mae, comemoracao, domingo, comemorar

**Topic 02**: mulheres, parabens, guerreiras, dia, mundo, luta, brasil, vida, especial, incriveis, marco, fortes, vcs, dias, direitos

**Topic 03**: feliz, dia, guerreiras, dias, mundo, vcs, incriveis, mulheres, bom, maravilhosas, deseja, merecem, lindas, desejar, linda

**Topic 04**: hoje, ser, sororidade, dias, luta, respeito, sempre, tudo, quero, gente, sim, vai, ter, sobre, homem

**Topic 05**: amo, video, tanto, demais, vida, tudo, deus, melhor, perfeito, mae, serie, nunca, sempre, amor, vcs

### 5x20

**Topic 01**: mulher, internacional, dia, marco, homenagem, parabens, bom, especial, via, hoje, amanha, mae, comemoracao, domingo, comemorar, presente, data, lugar, vida, forca

**Topic 02**: mulheres, parabens, guerreiras, dia, mundo, luta, brasil, vida, especial, incriveis, marco, fortes, vcs, dias, direitos, homenagem, maravilhosas, lindas, contra, merecem

**Topic 03**: feliz, dia, guerreiras, dias, mundo, vcs, incriveis, mulheres, bom, maravilhosas, deseja, merecem, lindas, desejar, linda, fortes, sempre, desejamos, amor, mulherada

**Topic 04**: hoje, ser, sororidade, dias, luta, respeito, sempre, tudo, quero, gente, sim, vai, ter, sobre, homem, flores, ano, pode, ainda, igualdade

**Topic 05**: amo, video, tanto, demais, vida, tudo, deus, melhor, perfeito, mae, serie, nunca, sempre, amor, vcs, vou, linda, cara, pqp, mulher

## 10 TOPICOS

### 10x5

**Topic 01**: mulher, internacional, dia, marco, homenagem

**Topic 02**: mulheres, dia, vida, guerreiras, luta

**Topic 03**: feliz, dia, guerreiras, vcs, dias

**Topic 04**: ser, mulher, quero, vai, homem

**Topic 05**: parabens, guerreiras, dia, vcs, marco

**Topic 06**: amo, tanto, demais, vida, deus

**Topic 07**: hoje, dias, luta, dia, respeito

**Topic 08**: sororidade, ter, marcela, gente, feminismo

**Topic 09**: video, tudo, perfeito, melhor, nunca

**Topic 10**: mundo, melhor, vcs, merecem, guerreiras

### 10x10

**Topic 01**: mulher, internacional, dia, marco, homenagem, especial, bom, via, amanha, mae

**Topic 02**: mulheres, dia, vida, guerreiras, luta, homenagem, incriveis, direitos, fortes, contra

**Topic 03**: feliz, dia, guerreiras, vcs, dias, bom, maravilhosas, incriveis, deseja, lindas

**Topic 04**: ser, mulher, quero, vai, homem, tudo, pode, vida, quiser, sim

**Topic 05**: parabens, guerreiras, dia, vcs, marco, especial, deus, bom, lindas, brasil

**Topic 06**: amo, tanto, demais, vida, deus, mae, amor, serie, linda, vcs

**Topic 07**: hoje, dias, luta, dia, respeito, sempre, igualdade, ano, direitos, flores

**Topic 08**: sororidade, ter, marcela, gente, feminismo, sobre, cara, empatia, caralho, amiga

**Topic 09**: video, tudo, perfeito, melhor, nunca, sempre, bom, vou, ver, vez

**Topic 10**: mundo, melhor, vcs, merecem, guerreiras, tudo, amor, brasil, respeito, forca

### 10x15

**Topic 01**: mulher, internacional, dia, marco, homenagem, especial, bom, via, amanha, mae, domingo, comemoracao, comemorar, presente, vida

**Topic 02**: mulheres, dia, vida, guerreiras, luta, homenagem, incriveis, direitos, fortes, contra, especial, brasil, homens, marco, outras

**Topic 03**: feliz, dia, guerreiras, vcs, dias, bom, maravilhosas, incriveis, deseja, lindas, desejar, merecem, linda, fortes, sempre

**Topic 04**: ser, mulher, quero, vai, homem, tudo, pode, vida, quiser, sim, sempre, ter, gente, deve, fazer

**Topic 05**: parabens, guerreiras, dia, vcs, marco, especial, deus, bom, lindas, brasil, dias, mulherada, sempre, batalhadoras, fortes

**Topic 06**: amo, tanto, demais, vida, deus, mae, amor, serie, linda, vcs, obrigada, mulher, musica, cara, caralho

**Topic 07**: hoje, dias, luta, dia, respeito, sempre, igualdade, ano, direitos, flores, bom, outros, sim, queremos, data

**Topic 08**: sororidade, ter, marcela, gente, feminismo, sobre, cara, empatia, caralho, amiga, nao, cade, aqui, feminista, porrada

**Topic 09**: video, tudo, perfeito, melhor, nunca, sempre, bom, vou, ver, vez, vejo, maravilhoso, deus, incrivel, faz

**Topic 10**: mundo, melhor, vcs, merecem, guerreiras, tudo, amor, brasil, respeito, forca, incriveis, gente, merece, pessoas, lugar

### 10x20

**Topic 01**: mulher, internacional, dia, marco, homenagem, especial, bom, via, amanha, mae, domingo, comemoracao, comemorar, presente, vida, sobre, lugar, data, homem, quiser

**Topic 02**: mulheres, dia, vida, guerreiras, luta, homenagem, incriveis, direitos, fortes, contra, especial, brasil, homens, marco, outras, maravilhosas, aqui, sobre, bolsonaro, historia

**Topic 03**: feliz, dia, guerreiras, vcs, dias, bom, maravilhosas, incriveis, deseja, lindas, desejar, merecem, linda, fortes, sempre, desejamos, mulherada, seguidoras, brazil, amigas

**Topic 04**: ser, mulher, quero, vai, homem, tudo, pode, vida, quiser, sim, sempre, ter, gente, deve, fazer, nunca, lugar, amor, ainda, assim

**Topic 05**: parabens, guerreiras, dia, vcs, marco, especial, deus, bom, lindas, brasil, dias, mulherada, sempre, batalhadoras, fortes, principalmente, maravilhosas, abencoe, guerreira, meninas

**Topic 06**: amo, tanto, demais, vida, deus, mae, amor, serie, linda, vcs, obrigada, mulher, musica, cara, caralho, perfeita, cena, cher, admiro, homem

**Topic 07**: hoje, dias, luta, dia, respeito, sempre, igualdade, ano, direitos, flores, bom, outros, sim, queremos, data, lembrar, vamos, marco, porque, apenas

**Topic 08**: sororidade, ter, marcela, gente, feminismo, sobre, cara, empatia, caralho, amiga, nao, cade, aqui, feminista, porrada, falar, vai, deus, vou, faz

**Topic 09**: video, tudo, perfeito, melhor, nunca, sempre, bom, vou, ver, vez, vejo, maravilhoso, deus, incrivel, faz, sobre, lindo, tanto, simplesmente, queria

**Topic 10**: mundo, melhor, vcs, merecem, guerreiras, tudo, amor, brasil, respeito, forca, incriveis, gente, merece, pessoas, lugar, cada, deus, homens, assim, maior

## 15 TOPICOS

### 15x5

**Topic 01**: mulher, internacional, dia, homenagem, via

**Topic 02**: mulheres, vida, incriveis, homenagem, homens

**Topic 03**: feliz, dia, dias, deseja, incriveis

**Topic 04**: ser, vai, homem, mulher, sempre

**Topic 05**: parabens, especial, deus, mulherada, brasil

**Topic 06**: amo, tanto, demais, vida, mae

**Topic 07**: hoje, dias, respeito, sempre, ano

**Topic 08**: sororidade, ter, marcela, feminismo, sobre

**Topic 09**: video, melhor, perfeito, sempre, nunca

**Topic 10**: luta, direitos, igualdade, contra, respeito

**Topic 11**: mundo, melhor, merecem, vcs, amor

**Topic 12**: tudo, quero, sim, gente, flores

**Topic 13**: dia, bom, internacional, especial, cada

**Topic 14**: guerreiras, vcs, lindas, fortes, dias

**Topic 15**: marco, homenagem, data, respeito, ano

### 15x10

**Topic 01**: mulher, internacional, dia, homenagem, via, mae, vida, lugar, quiser, amanha

**Topic 02**: mulheres, vida, incriveis, homenagem, homens, especial, fortes, brasil, outras, maravilhosas

**Topic 03**: feliz, dia, dias, deseja, incriveis, maravilhosas, desejar, sempre, linda, vcs

**Topic 04**: ser, vai, homem, mulher, sempre, pode, quiser, ter, vida, deve

**Topic 05**: parabens, especial, deus, mulherada, brasil, sempre, guerreira, meninas, abencoe, amor

**Topic 06**: amo, tanto, demais, vida, mae, deus, amor, serie, linda, vcs

**Topic 07**: hoje, dias, respeito, sempre, ano, outros, porque, maior, ontem, receber

**Topic 08**: sororidade, ter, marcela, feminismo, sobre, gente, cara, empatia, caralho, amiga

**Topic 09**: video, melhor, perfeito, sempre, nunca, bom, ver, vou, vez, vejo

**Topic 10**: luta, direitos, igualdade, contra, respeito, vamos, violencia, sempre, data, ruas

**Topic 11**: mundo, melhor, merecem, vcs, amor, brasil, merece, forca, pessoas, incriveis

**Topic 12**: tudo, quero, sim, gente, flores, respeito, faz, bombom, dar, queremos

**Topic 13**: dia, bom, internacional, especial, cada, comemoracao, comemorar, lembrar, otimo, celebrar

**Topic 14**: guerreiras, vcs, lindas, fortes, dias, maravilhosas, batalhadoras, merecem, incriveis, brasil

**Topic 15**: marco, homenagem, data, respeito, ano, dia, forca, comemorado, mes, internacional

### 15x15

**Topic 01**: mulher, internacional, dia, homenagem, via, mae, vida, lugar, quiser, amanha, sobre, onde, casa, homem, deus

**Topic 02**: mulheres, vida, incriveis, homenagem, homens, especial, fortes, brasil, outras, maravilhosas, aqui, contra, trans, marcha, fazem

**Topic 03**: feliz, dia, dias, deseja, incriveis, maravilhosas, desejar, sempre, linda, vcs, desejamos, respeito, amor, mulherada, seguidoras

**Topic 04**: ser, vai, homem, mulher, sempre, pode, quiser, ter, vida, deve, assim, nunca, orgulho, fazer, ainda

**Topic 05**: parabens, especial, deus, mulherada, brasil, sempre, guerreira, meninas, abencoe, amor, linda, principalmente, dar, vcs, brazil

**Topic 06**: amo, tanto, demais, vida, mae, deus, amor, serie, linda, vcs, obrigada, musica, cara, caralho, homem

**Topic 07**: hoje, dias, respeito, sempre, ano, outros, porque, maior, ontem, receber, apenas, flores, presente, queremos, especial

**Topic 08**: sororidade, ter, marcela, feminismo, sobre, gente, cara, empatia, caralho, amiga, nao, cade, feminista, porrada, vai

**Topic 09**: video, melhor, perfeito, sempre, nunca, bom, ver, vou, vez, vejo, maravilhoso, incrivel, deus, sobre, lindo

**Topic 10**: luta, direitos, igualdade, contra, respeito, vamos, violencia, sempre, data, ruas, forca, sobre, resistencia, ainda, machismo

**Topic 11**: mundo, melhor, merecem, vcs, amor, brasil, merece, forca, pessoas, incriveis, respeito, lugar, gente, assim, homens

**Topic 12**: tudo, quero, sim, gente, flores, respeito, faz, bombom, dar, queremos, aqui, chocolate, fale, pode, queria

**Topic 13**: dia, bom, internacional, especial, cada, comemoracao, comemorar, lembrar, otimo, celebrar, nada, domingo, desejo, amanha, melhor

**Topic 14**: guerreiras, vcs, lindas, fortes, dias, maravilhosas, batalhadoras, merecem, incriveis, brasil, lutam, maes, nunca, forca, cada

**Topic 15**: marco, homenagem, data, respeito, ano, dia, forca, comemorado, mes, internacional, flores, maior, oito, greve, fabrica

### 15x20

**Topic 01**: mulher, internacional, dia, homenagem, via, mae, vida, lugar, quiser, amanha, sobre, onde, casa, homem, deus, brasil, presente, guerreira, linda, pleno

**Topic 02**: mulheres, vida, incriveis, homenagem, homens, especial, fortes, brasil, outras, maravilhosas, aqui, contra, trans, marcha, fazem, inspiram, negras, historia, direitos, bolsonaro

**Topic 03**: feliz, dia, dias, deseja, incriveis, maravilhosas, desejar, sempre, linda, vcs, desejamos, respeito, amor, mulherada, seguidoras, brazil, lindas, merecem, rainhas, amigas

**Topic 04**: ser, vai, homem, mulher, sempre, pode, quiser, ter, vida, deve, assim, nunca, orgulho, fazer, ainda, forte, deveria, amor, sabe, nada

**Topic 05**: parabens, especial, deus, mulherada, brasil, sempre, guerreira, meninas, abencoe, amor, linda, principalmente, dar, vcs, brazil, esposa, forca, boa, respeito, noite

**Topic 06**: amo, tanto, demais, vida, mae, deus, amor, serie, linda, vcs, obrigada, musica, cara, caralho, homem, cena, cher, perfeita, admiro, foto

**Topic 07**: hoje, dias, respeito, sempre, ano, outros, porque, maior, ontem, receber, apenas, flores, presente, queremos, especial, dar, lembrar, data, igualdade, anos

**Topic 08**: sororidade, ter, marcela, feminismo, sobre, gente, cara, empatia, caralho, amiga, nao, cade, feminista, porrada, vai, aqui, falar, outras, obrigada, vou

**Topic 09**: video, melhor, perfeito, sempre, nunca, bom, ver, vou, vez, vejo, maravilhoso, incrivel, deus, sobre, lindo, tanto, vezes, morrer, completo, simplesmente

**Topic 10**: luta, direitos, igualdade, contra, respeito, vamos, violencia, sempre, data, ruas, forca, sobre, resistencia, ainda, machismo, diaria, genero, continua, sociedade, lembrar

**Topic 11**: mundo, melhor, merecem, vcs, amor, brasil, merece, forca, pessoas, incriveis, respeito, lugar, gente, assim, homens, cada, maior, onde, metade, ainda

**Topic 12**: tudo, quero, sim, gente, flores, respeito, faz, bombom, dar, queremos, aqui, chocolate, fale, pode, queria, amor, deus, fazer, bem, flor

**Topic 13**: dia, bom, internacional, especial, cada, comemoracao, comemorar, lembrar, otimo, celebrar, nada, domingo, desejo, amanha, melhor, porra, porque, lindo, apenas, machista

**Topic 14**: guerreiras, vcs, lindas, fortes, dias, maravilhosas, batalhadoras, merecem, incriveis, brasil, lutam, maes, nunca, forca, cada, deus, verdadeiras, amigas, duas, sempre

**Topic 15**: marco, homenagem, data, respeito, ano, dia, forca, comemorado, mes, internacional, flores, maior, oito, greve, fabrica, delicadeza, lutas, dias, amor, historia

## 20 TOPICOS

### 20x5

**Topic 01**: mulher, internacional, dia, bom, via

**Topic 02**: mulheres, dia, vida, incriveis, especial

**Topic 03**: feliz, dia, deseja, incriveis, desejar

**Topic 04**: sobre, vai, aqui, homem, nada

**Topic 05**: parabens, especial, brasil, mulherada, meninas

**Topic 06**: amo, tanto, demais, vida, serie

**Topic 07**: hoje, dia, bom, especial, ontem

**Topic 08**: sororidade, marcela, ter, empatia, feminismo

**Topic 09**: video, perfeito, melhor, nunca, bom

**Topic 10**: ser, mulher, pode, deve, quiser

**Topic 11**: mundo, melhor, merecem, vcs, brasil

**Topic 12**: quero, sim, flores, bombom, respeito

**Topic 13**: luta, direitos, igualdade, contra, dia

**Topic 14**: guerreiras, vcs, dia, lindas, fortes

**Topic 15**: marco, dia, data, comemorado, ano

**Topic 16**: tudo, faz, bom, acima, bem

**Topic 17**: homenagem, linda, especial, forca, singela

**Topic 18**: gente, linda, coisa, quer, bom

**Topic 19**: dias, respeito, ano, outros, queremos

**Topic 20**: sempre, deus, vida, amor, abencoe

### 20x10

**Topic 01**: mulher, internacional, dia, bom, via, especial, mae, amanha, comemoracao, comemorar

**Topic 02**: mulheres, dia, vida, incriveis, especial, fortes, outras, homens, brasil, maravilhosas

**Topic 03**: feliz, dia, deseja, incriveis, desejar, maravilhosas, linda, vcs, desejamos, mulherada

**Topic 04**: sobre, vai, aqui, homem, nada, ainda, fazer, nunca, ter, vamos

**Topic 05**: parabens, especial, brasil, mulherada, meninas, guerreira, dia, principalmente, vcs, dar

**Topic 06**: amo, tanto, demais, vida, serie, mae, vcs, obrigada, musica, linda

**Topic 07**: hoje, dia, bom, especial, ontem, porque, outros, celebrar, lembrar, aniversario

**Topic 08**: sororidade, marcela, ter, empatia, feminismo, caralho, cara, cade, amiga, porrada

**Topic 09**: video, perfeito, melhor, nunca, bom, vez, ver, maravilhoso, vejo, vou

**Topic 10**: ser, mulher, pode, deve, quiser, vida, deveria, orgulho, forte, homem

**Topic 11**: mundo, melhor, merecem, vcs, brasil, merece, forca, incriveis, pessoas, amor

**Topic 12**: quero, sim, flores, bombom, respeito, chocolate, fale, dar, flor, queremos

**Topic 13**: luta, direitos, igualdade, contra, dia, violencia, data, ruas, forca, resistencia

**Topic 14**: guerreiras, vcs, dia, lindas, fortes, bom, maravilhosas, batalhadoras, merecem, incriveis

**Topic 15**: marco, dia, data, comemorado, ano, mes, oito, greve, lutas, forca

**Topic 16**: tudo, faz, bom, acima, bem, queria, obrigado, podemos, cena, simplesmente

**Topic 17**: homenagem, linda, especial, forca, singela, melhor, maior, respeito, bela, coisa

**Topic 18**: gente, linda, coisa, quer, bom, olha, demais, muita, vem, encontra

**Topic 19**: dias, respeito, ano, outros, queremos, merecem, igualdade, flores, amor, apenas

**Topic 20**: sempre, deus, vida, amor, abencoe, forca, mae, melhor, obrigada, coracao

### 20x15

**Topic 01**: mulher, internacional, dia, bom, via, especial, mae, amanha, comemoracao, comemorar, presente, domingo, pleno, mensagem, lugar

**Topic 02**: mulheres, dia, vida, incriveis, especial, fortes, outras, homens, brasil, maravilhosas, direitos, trans, marcha, contra, negras

**Topic 03**: feliz, dia, deseja, incriveis, desejar, maravilhosas, linda, vcs, desejamos, mulherada, seguidoras, brazil, lindas, rainhas, amigas

**Topic 04**: sobre, vai, aqui, homem, nada, ainda, fazer, nunca, ter, vamos, vou, dar, homens, falar, bem

**Topic 05**: parabens, especial, brasil, mulherada, meninas, guerreira, dia, principalmente, vcs, dar, cada, brazil, abencoe, esposa, pais

**Topic 06**: amo, tanto, demais, vida, serie, mae, vcs, obrigada, musica, linda, caralho, amor, mulher, admiro, cara

**Topic 07**: hoje, dia, bom, especial, ontem, porque, outros, celebrar, lembrar, aniversario, anos, celebramos, data, entao, homenagear

**Topic 08**: sororidade, marcela, ter, empatia, feminismo, caralho, cara, cade, amiga, porrada, feminista, nao, seletiva, outras, obrigada

**Topic 09**: video, perfeito, melhor, nunca, bom, vez, ver, maravilhoso, vejo, vou, incrivel, tanto, completo, lindo, morrer

**Topic 10**: ser, mulher, pode, deve, quiser, vida, deveria, orgulho, forte, homem, ter, humano, especial, sabe, merece

**Topic 11**: mundo, melhor, merecem, vcs, brasil, merece, forca, incriveis, pessoas, amor, lugar, metade, cada, inteiro, maior

**Topic 12**: quero, sim, flores, bombom, respeito, chocolate, fale, dar, flor, queremos, bombons, promocao, pode, aceito, presente

**Topic 13**: luta, direitos, igualdade, contra, dia, violencia, data, ruas, forca, resistencia, vamos, diaria, continua, genero, machismo

**Topic 14**: guerreiras, vcs, dia, lindas, fortes, bom, maravilhosas, batalhadoras, merecem, incriveis, brasil, maes, lutam, cada, verdadeiras

**Topic 15**: marco, dia, data, comemorado, ano, mes, oito, greve, lutas, forca, fabrica, conquistas, domingo, flores, ruas

**Topic 16**: tudo, faz, bom, acima, bem, queria, obrigado, podemos, cena, simplesmente, fazer, pouco, absolutamente, vida, fez

**Topic 17**: homenagem, linda, especial, forca, singela, melhor, maior, respeito, bela, coisa, delicadeza, sabedoria, pequena, receber, transforma

**Topic 18**: gente, linda, coisa, quer, bom, olha, demais, muita, vem, encontra, tanto, fala, assim, sabe, boa

**Topic 19**: dias, respeito, ano, outros, queremos, merecem, igualdade, flores, amor, apenas, maior, receber, lutam, lutamos, admiracao

**Topic 20**: sempre, deus, vida, amor, abencoe, forca, mae, melhor, obrigada, coracao, cada, respeito, razao, lembrar, mulher

### 20x20

**Topic 01**: mulher, internacional, dia, bom, via, especial, mae, amanha, comemoracao, comemorar, presente, domingo, pleno, mensagem, lugar, vida, aniversario, casa, guerreira, otimo

**Topic 02**: mulheres, dia, vida, incriveis, especial, fortes, outras, homens, brasil, maravilhosas, direitos, trans, marcha, contra, negras, inspiram, fazem, bolsonaro, pras, historia

**Topic 03**: feliz, dia, deseja, incriveis, desejar, maravilhosas, linda, vcs, desejamos, mulherada, seguidoras, brazil, lindas, rainhas, amigas, merecem, meninas, fodas, bom, foda

**Topic 04**: sobre, vai, aqui, homem, nada, ainda, fazer, nunca, ter, vamos, vou, dar, homens, falar, bem, ver, onde, lugar, feminismo, quiser

**Topic 05**: parabens, especial, brasil, mulherada, meninas, guerreira, dia, principalmente, vcs, dar, cada, brazil, abencoe, esposa, pais, boa, vez, forca, obrigado, noite

**Topic 06**: amo, tanto, demais, vida, serie, mae, vcs, obrigada, musica, linda, caralho, amor, mulher, admiro, cara, perfeita, cena, cher, pqp, foda

**Topic 07**: hoje, dia, bom, especial, ontem, porque, outros, celebrar, lembrar, aniversario, anos, celebramos, data, entao, homenagear, apenas, comemorar, ano, amanha, parabenizar

**Topic 08**: sororidade, marcela, ter, empatia, feminismo, caralho, cara, cade, amiga, porrada, feminista, nao, seletiva, outras, obrigada, dificil, palavra, mina, existe, umas

**Topic 09**: video, perfeito, melhor, nunca, bom, vez, ver, maravilhoso, vejo, vou, incrivel, tanto, completo, lindo, morrer, simplesmente, pqp, vezes, canso, ronaldinho

**Topic 10**: ser, mulher, pode, deve, quiser, vida, deveria, orgulho, forte, homem, ter, humano, especial, sabe, merece, precisa, forca, livre, princesa, rainha

**Topic 11**: mundo, melhor, merecem, vcs, brasil, merece, forca, incriveis, pessoas, amor, lugar, metade, cada, inteiro, maior, fazem, homens, onde, assim, justo

**Topic 12**: quero, sim, flores, bombom, respeito, chocolate, fale, dar, flor, queremos, bombons, promocao, pode, aceito, presente, desejar, mimos, mensagem, ganhar, foda

**Topic 13**: luta, direitos, igualdade, contra, dia, violencia, data, ruas, forca, resistencia, vamos, diaria, continua, genero, machismo, respeito, sociedade, lembrar, democracia, cada

**Topic 14**: guerreiras, vcs, dia, lindas, fortes, bom, maravilhosas, batalhadoras, merecem, incriveis, brasil, maes, lutam, cada, verdadeiras, forca, amigas, duas, nunca, lutadoras

**Topic 15**: marco, dia, data, comemorado, ano, mes, oito, greve, lutas, forca, fabrica, conquistas, domingo, flores, ruas, historia, origem, anualmente, delicadeza, viva

**Topic 16**: tudo, faz, bom, acima, bem, queria, obrigado, podemos, cena, simplesmente, fazer, pouco, absolutamente, vida, fez, mae, vcs, falou, cher, maravilhosas

**Topic 17**: homenagem, linda, especial, forca, singela, melhor, maior, respeito, bela, coisa, delicadeza, sabedoria, pequena, receber, transforma, encantador, faz, amor, suave, obrigada

**Topic 18**: gente, linda, coisa, quer, bom, olha, demais, muita, vem, encontra, tanto, fala, assim, sabe, boa, serio, maravilhosa, faz, pode, fofo

**Topic 19**: dias, respeito, ano, outros, queremos, merecem, igualdade, flores, amor, apenas, maior, receber, lutam, lutamos, admiracao, carinho, merecemos, reconhecimento, presente, inspiram

**Topic 20**: sempre, deus, vida, amor, abencoe, forca, mae, melhor, obrigada, coracao, cada, respeito, razao, lembrar, mulher, desde, fortes, carinho, incrivel, nunca