# Resultados da modelagem de tópicos usando NMF

## 2020

### Cenário 1
* [Português](2020-c1-pt.md)
* [Inglês](2020-c1-en.md)
* [Espanhol](2020-c1-es.md)

### Cenário 2
* [Português](2020-c2-pt.md)
* [Inglês](2020-c2-en.md)
* [Espanhol](2020-c2-es.md)

## 2021

### Cenário 1
* [Português](2021-c1-pt.md)
* [Inglês](2021-c1-en.md)
* [Espanhol](2021-c1-es.md)

### Cenário 2
* [Português](2021-c2-pt.md)
* [Inglês](2021-c2-en.md)
* [Espanhol](2021-c2-es.md)
